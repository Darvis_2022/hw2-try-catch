const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

let div;
let ul;
let i;

div = document.createElement('div');
div.setAttribute('id', 'root');
document.body.appendChild(div);
ul = document.createElement('ul');
document.getElementById('root').appendChild(ul);
for (let _i = 0, books_1 = books; _i < books_1.length; _i++) {
    let element = books_1[_i];
    try {
        if (!element.author) {
            throw new SyntaxError('Без автора');
        }
        else if (!element.name) {
            throw new SyntaxError('Без імені');
        }
        else if (!element.price) {
            throw new SyntaxError('Без ціни');
        }
        else {
            var listItem = document.createElement('li');
            listItem.innerHTML = JSON.stringify(element, null, '  ');
            ul.append(listItem);
        }
    }
    catch (err) {
        console.log(err);
    }
}
